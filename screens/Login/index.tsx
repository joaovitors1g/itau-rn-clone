import React from 'react';
import UserBar from '../../components/UserBar';
import LoginForm from '../../components/LoginForm';
import BottomTabs from '../../components/BottomTabs';
import {LoginBackground} from './styles';
import {View} from 'react-native';

const Login: React.FC = () => {
  return (
    <LoginBackground>
      <View
        style={{
          flex: 1,
        }}>
        <UserBar />
        <LoginForm />
      </View>
      <BottomTabs />
    </LoginBackground>
  );
};

export default Login;
