import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';

export const LoginBackground = styled(LinearGradient).attrs({
  colors: ['#ff801f', '#ffbc1f'],
})`
  flex: 1;
`;
