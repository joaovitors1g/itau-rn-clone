import styled from 'styled-components/native'

export const Container = styled.View`
  margin-top: 20px;
`

export const KeysGrid = styled.View`
  flex-direction: row;
  margin-bottom: 10px;
`

export const Key = styled.TouchableWithoutFeedback``

export const KeyText = styled.Text``

export const TouchableView = styled.View`
  background-color: #fff;
  width: 50px;
  height: 50px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  margin-left: 10px;
`
