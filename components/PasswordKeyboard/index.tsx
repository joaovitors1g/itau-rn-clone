import React from 'react'
import { View } from 'react-native'

import { Container, KeysGrid, Key, TouchableView, KeyText } from './styles'

const PasswordKeyboard: React.FC = () => {
  return (
    <Container>
      <KeysGrid>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
      </KeysGrid>
      <KeysGrid>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
        <Key onPress={() => {}}>
          <TouchableView>
            <KeyText>1 ou 3</KeyText>
          </TouchableView>
        </Key>
      </KeysGrid>
    </Container>
  )
}

export default PasswordKeyboard
