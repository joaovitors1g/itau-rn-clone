import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Tab = styled.TouchableOpacity`
  align-items: center;
  padding: 15px 40px;
`;

export const TabLabel = styled.Text`
  color: #fff;
  margin-top: 10px;
`;
