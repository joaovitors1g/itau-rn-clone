import React from 'react';
import {useNavigation} from '@react-navigation/native';

import Icon from 'react-native-vector-icons/Feather';

import {Container, Tab, TabLabel} from './styles';

const BottomTabs: React.FC = () => {
  const navigation = useNavigation();

  return (
    <Container>
      <Tab onPress={() => navigation.navigate('Atalhos')}>
        <Icon name="arrow-left" size={32} color="#fff" />
        <TabLabel>atalhos</TabLabel>
      </Tab>
      <Tab onPress={() => navigation.navigate('iToken')}>
        <Icon name="shield" size={32} color="#fff" />
        <TabLabel>iToken</TabLabel>
      </Tab>
      <Tab onPress={() => navigation.navigate('Ajuda')}>
        <Icon name="help-circle" size={32} color="#fff" />
        <TabLabel>ajuda</TabLabel>
      </Tab>
    </Container>
  );
};

export default BottomTabs;
