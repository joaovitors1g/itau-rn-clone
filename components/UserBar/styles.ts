import styled from 'styled-components/native';

export const HeaderWrapper = styled.View`
  background-color: #fff;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 14px 18px;
`;

export const UserInfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const UserAvatar = styled.Image`
  border-radius: 25px;
  margin-right: 20px;
  width: 35px;
  height: 35px;
`;

export const UserAccountInfo = styled.View``;

export const WelcomeMessage = styled.Text`
  color: #ff895e;
  font-weight: bold;
  font-family: 'Roboto';
  font-size: 16px;
`;

export const AccountInfo = styled.Text`
  color: #000;
  font-weight: 600;
`;
