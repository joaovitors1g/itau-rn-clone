import { RectButton } from 'react-native-gesture-handler'

import styled from 'styled-components/native'

export const Container = styled.View`
  align-items: center;
  padding-top: 26px;
  width: 60%;
  margin: 0 auto;
`

export const FieldLabel = styled.Text`
  color: #fff;
  font-size: 18px;
`

export const InputGroup = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  border-bottom-color: #fff;
  border-bottom-width: 2px;
`

export const PasswordInput = styled.TextInput`
  width: 100%;
  margin-left: 10px;
`

export const LoginButton = styled(RectButton)`
  background-color: #fff;
  width: 100%;
  height: 50px;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  border-radius: 5px;
`

export const LoginButtonText = styled.Text`
  color: #ff895e;
  font-weight: bold;
`

export const ForgotPasswordText = styled.Text`
  color: #fff;
  font-size: 16px;
  margin-top: 20px;
`
