import React, { useState } from 'react'
import { ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

import PasswordKeyboard from '../PasswordKeyboard'
import {
  Container,
  FieldLabel,
  InputGroup,
  PasswordInput,
  LoginButton,
  LoginButtonText,
  ForgotPasswordText
} from './styles'

const LoginForm: React.FC = () => {
  const [loading, setLoading] = useState(false)

  function showPasswordKeyboard() {
    setLoading(true)

    setTimeout(() => {
      setLoading(false)
    }, 3000)
  }

  return (
    <Container>
      <FieldLabel>senha eletrônica</FieldLabel>
      <InputGroup>
        <Icon name="lock" size={24} color="#fff" />
        <PasswordInput editable={false} />
      </InputGroup>
      <PasswordKeyboard />
      <LoginButton onPress={showPasswordKeyboard}>
        {loading ? (
          <ActivityIndicator color="#42cbf5" />
        ) : (
          <LoginButtonText>acessar</LoginButtonText>
        )}
      </LoginButton>
      <ForgotPasswordText>esqueci minha senha</ForgotPasswordText>
    </Container>
  )
}

export default LoginForm
