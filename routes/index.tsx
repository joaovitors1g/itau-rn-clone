import React from 'react'
import { Text } from 'react-native'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Login from '../screens/Login'

const Stack = createStackNavigator()

const Atalhos: React.FC = () => <Text>Atalhos</Text>
const IToken: React.FC = () => <Text>iToken</Text>
const Ajuda: React.FC = () => <Text>Ajuda</Text>

const Routes: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Atalhos" component={Atalhos} />
        <Stack.Screen name="iToken" component={IToken} />
        <Stack.Screen name="Ajuda" component={Ajuda} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Routes
